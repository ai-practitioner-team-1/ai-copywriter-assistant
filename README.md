# AI CopyWriter Assistant using GPT-Neo

![title](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/title.jpg)

	NLP Contextual Text Generator for Ad Copy Samples
# IBM IMVAI-0904 AI Practitioner Team 1
| Team Member  |Tasks  |
| --- |---|
| **Tan Shung Sin (Song)**| PowerPoint Design, Editing & Layout, Data Cleaning, EDT |
| **Tan Liam An** | Data Curation: cataloguing, conversion, cleaning and archiving  |
| **Wu Wen** |Python Coding, Web App Design, Testing, Training|
| **Lawrence Wong** | Research, System Architecture Design, Project Management, Debugging |


## List of folders included:

### Presentation
	Project PowerPoint

[Download PowerPoint](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/presentation/Team_1_AI_CopyWriter.pptx)

### AI CopyWriter notebook

	Juypter notebook of Python codes for Happy Transformer
This GitLab Repo contains the supporting Jupyter-notebooks for the project covering GPT-Neo Model training, testing and deployment.    The codes has following parts:
### 1-Click Notebooks 


| Description     |      1-Click Notebook      |
|:----------|------|
| AI Project Copywriter R9.ipynb  -    Train and Evaluation for 30 epochs | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI%20Project%20Copywriter%20R9.ipynb) |
|  AI Project Copywriter R9A.ipynb  - Train and Evaluation for 9 epochs  | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI%20Project%20Copywriter%20R9A.ipynb) |
| AI _Copywriter_R10_Trainer.ipynb  -   Final Training / Saving Model | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI_Copywriter_Trainer.ipynb) |
| AI_Copywriter_Deploy.ipynb  -    Deployment to Anvil | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI_Copywriter_Deploy.ipynb) |

The notebooks and links for the rest of the parts will be updated as and when they are published.


### Dataset

	All dataset of RAW Word Doc, Cleaned Text data

##### Copyright ©️ on contents of data by Arthur Carmazzi [www.carmazzi.net](www.caramzzi.net)
	All rights reserved in dataset.  
	Usage is allowed for educational / demonstration purposes only.
### Model (trained)
	GPT-Neo 125M Model fine tuned with Arthur's books

### Anvil.works code
[anvil/AI_Copywriter_Assistant](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/tree/main/anvil/AI_Copywriter_Assistant)

### Misc Test and Evaluation Results
[eval_results](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/tree/main/eval_result)

## Operating instructions
run AI_Copywriter_Deploy on the Paperspace GPU hosting [Gradient Paperspace](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI_Copywriter_Deploy.ipynb)
run Anvil.works webapp codes [Clone it here](https://anvil.works/build#clone:2ZRAYVLF3RA62L7Y=ISMDABMOX64T75JQLJGHVJLS)



### WebApp Anvil.works
	run http://bit.ly/AI-CopyWriter 
http://bit.ly/AI-CopyWriter




![Deployment](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/Deployment.gif)

#### Web App Screen Shot

![alt text](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/webapp1.png?raw=true)

![alt text](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/webapp2.png?raw=true)


## Training Process

![Development](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/Development.gif)

## Deployment

![Deploy](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/Deploy.gif)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Copyleft [MIT](https://choosealicense.com/licenses/mit/)

Copyright reserved for Dataset contents by Arthur Carmazzi
