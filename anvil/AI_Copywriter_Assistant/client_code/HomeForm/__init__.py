from ._anvil_designer import HomeFormTemplate
from anvil import *
import stripe.checkout
import anvil.server
from ..Introduction import Introduction
from ..TextGeneration import TextGeneration


class HomeForm(HomeFormTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)
      
    # For Now:
    self.link_intro_click()

  def link_intro_click(self, **event_args):
    """This method is called when the link is clicked"""
    self.link_intro.role = "selected"
    self.load_component(Introduction())

  def link_text_click(self, **event_args):
    """This method is called when the link is clicked"""
    self.load_component(TextGeneration())


  def load_component(self,cmpt):
    self.column_panel_content.clear()
    self.column_panel_content.add_component(cmpt)
