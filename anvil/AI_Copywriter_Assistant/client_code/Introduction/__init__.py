from ._anvil_designer import IntroductionTemplate
from anvil import *
import stripe.checkout
import anvil.server

class Introduction(IntroductionTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)

    # Any code you write here will run when the form opens.