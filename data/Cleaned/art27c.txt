Why Speed is Essential in Modern Organizational Culture Change Consulting

There are many organizational culture initiatives and many strategies related to organizational culture change. The evolution of culture change is also an important factor… technology has had a significant impact on the culture change processes that work. As described in the comprehensive Ultimate Guide to Organizational Culture Change manuscript, speed of results is essential for sustainability. 

Organizational initiatives used to need more time to implement and, while employees may not have been fully engaged in the process, persistence in a change initiative eventually paid off… sometimes up to 6 years later. 

But now we live in the PFB era (Post Facebook) and our patience is shorter and our expectations higher. We want instant results… and if we do not get them, we quickly get disengaged. So if we are to create a sustainable organizational culture change initiative, we need to show at least some results in the first week of the project and continue to show more positive results as the program continues. This used to be near impossible, but now, not only has this been proven across multiple organizations across the globe, but it is essential for success.

Technology and the latest breakthroughs in organizational psychology now support fast results… but there is criteria for this to have these fast results. 

, Groups of larger than 1200 people must be broken down to divisions or segments

, Senior management Must be actively involved and spend 3 hours each day for 5 days during a 2 week period

, The organization must be willing to let go of or move negative influencers that have affect the organizational culture

, The organization must be prepared to have a disruption of operations during a 2 week period 

, The initial implementation process cannot take more than 3 weeks

The tradition of long-term culture change initiatives is based on old models that pre-date the level of technology we currently are in and consistently expanding. People (at least most) have become more adaptable, and adopting change more readily IF they feel it benefits them… and this is the key!

Fast organizational culture change works IF you can quickly show a beneficial result to the stakeholders. But to do this, there needs to be a frame and emotional connection before we begin. This frame begins with a greater purpose (one of the 5 pillars of organizational change described in the comprehensive Ultimate Guide to Organizational Culture Change). The greater purpose is designed to provide a personal emotional and even altruistic benefit to the stake holder. But the real trick in achieving fast culture change is getting everyone to have the same “greater purpose” across the entire organization. 

…How?

Based on our research in 56 different countries with multiple cultures, education levels, and positions, when we ask the question: “What is your ideal work environment?”, we consistently get the same types of answers.

, Team work 

, A supportive environment, which relates to cooperation from all departments and levels of the organization

, Trust, which relates to topics like innovation and autonomy… management trusting you to do what you think is best… as well as accountability, being able to trust that people will follow through and do what they are supposed to.

, Clarity… an understanding of what and how to achieve what is expected, clear communication, vision, structure and direction or at least a clear objective. 

, Fun, actually enjoying the process of work, innovation and achievement with colleagues and team mates.

So it appears that most people have the same vision for the “Ideal Working Environment”, yet we do not usually achieve it. And the reason, even though we may have the same vision, we have different processes and usually are not focused on its creation. 

This becomes the Greater Purpose, the one thing everyone wants, everyone feels it will benefit them, and everyone believes that if they could really achieve it, they would have accomplished something bigger than themselves. Now you have the common focus of creating the IDEAL work environment that connects personal benefit to the culture change outcomes.

With the common goal in hand, there is now a common vision and hope that it is actually possible, at least enough to try. And so the pre-discovered informal influencers of the organization have ammunition to work with. The create Cells by connecting with 10 to 15 of the people who know and respect them and, although sceptical, apply the new processes with their cells while getting instant feedback. The feedback is reviewed with key influencers to hear the voice of the masses and include them. By the 4th day of implementation, people start to see results. Now everything that was promised seems possible. Excitement sets in. people now start to believe that “THEY” can create this ideal work environment which will translate to better work life fit and more potential for personal success through the organization. 

The speed of results has spurred and perpetuated the engagement in the Organizational Culture change program. Now the people (most of them) are anticipating the next learning and implementation since the Ideal Working Environment is getting closer and closer. 

This is base on real results in multiple countries with organizations in different industries. This is not theory, it is experience. To understand this culture change process more clearly, read the complete document Ultimate Guide to Organizational Culture Change.
