Client situations organizational culture and leadership

People are not excited about their work
* Deadlines are missed
* People miss work or come late
* People complain about many work related situations

People wait to be told what to do
* People do not come up with innovative ideas to solve problems
* People are not Proactive

People are often blaming others when things go wrong
* People spend more time trying to find out who made a mistake that they do solving the actual problem
* Much time is spent arguing about why someone made a mistake than learning how to prevent the mistake in the future

People Misunderstand each other
* People do not communicate effectively
* Conflict leads to even less communication and engagement
* Mistrust is created across the organization
* People keep quiet when they could provide valuable feedback
* Silos are created across departments
* Organizational engagement is low

There is little cross-departmental cooperation (silos)