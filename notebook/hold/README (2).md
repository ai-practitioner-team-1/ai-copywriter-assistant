# AI CopyWriter Assistant using GPT-Neo
NLP Contextual Text Generator for Ad Copy Samples
# IBM IMVAI-0904 AI Practitioner Team 1
## Team members: Tan Liam An, Wu Wen, Lawrence Wong, Tan Shung Sin

# AI CopyWriter Codes

This GitLab Repo contains the supporting Jupyter-notebooks for the project covering GPT-Neo Model training, testing and deployment.
The codes has following parts
## 1-Click Notebooks 


| Description     |      1-Click Notebook      |
|:----------|------|
|  AI Project Copywriter R9.ipynb | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/github/Paperspace/PyTorch-101-Tutorial-Series/blob/master/PyTorch%20101%20Part%201%20-%20Computational%20Graphs%20and%20Autograd%20in%20PyTorch.ipynb) |
|  AI Project Copywriter R9A.ipynb  | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/github/Paperspace/PyTorch-101-Tutorial-Series/blob/master/PyTorch%20101%20Part%202%20-%20Building%20Neural%20Networks%20With%20PyTorch.ipynb) |
|  AI _Copywriter_R10_Trainer.ipynb - Final Training / Saving Model| [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/github/Paperspace/PyTorch-101-Tutorial-Series/blob/master/PyTorch%20101%20Part%203%20-%20Advance%20PyTorch%20Usage.ipynb) |
|  AI_Copywriter_Deploy.ipynb - Deployment to Anvil | [![Gradient](https://assets.paperspace.io/img/gradient-badge.svg)](https://console.paperspace.com/tevomnvq7/notebook/rcw17z8afg1neet?file=notebook%2FAI_Copywriter_Deploy.ipynb) |

The notebooks and links for the rest of the parts will be updated as and when they are published.



## List of folders included:

### Presentation
	powerpoint of project
![alt text](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/presentation/Team_1_AI_CopyWriter.pptx
### notebook
	Juypter notebook of Python codes for Happy Transformer
    
### data
	all the RAW Word Doc, Cleaned Text data
### model
	GPT-Neo 125M Model fine tuned with Arthur's books

## Operating instructions
run AI_Copywriter_Deploy on a GPU hosting (PaperSpace)
#
WebApp is Anvil.works
#
![alt text](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/webapp1.png?raw=true)
#
![alt text](https://gitlab.com/ai-practitioner-team-1/ai-copywriter-assistant/-/raw/main/images/webapp2.png?raw=true)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
